.PHONY: analyze
analyze:
	python src/analyze_data.py
	python src/analyze_simulations.py