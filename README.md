# Inference for Losers replication code

## Environment setup

Click [here](https://gitpod.io/#https://gitlab.com/dsbowen/inference-for-losers/-/tree/main/) to run a pre-configured cloud environment with Gitpod.

If working locally, you'll need python-3.8 or higher. Set up your environment with:

```bash
$ pip install -r requirements.txt
```

## Data

Download the data files from the AEA website and put them in a folder named `data`. There should be 3 files:

- `movers.csv` contains the conventional estimates and covariance matrix for the movers dataset (Chetty & Hendren, 2018)
- `oa.csv` contains the conventional estimates and covariance matrix for the Opportunity Atlas (OA) dataset (Chetty et al., 2018)
- `simulation_results.csv` contains the simulation results

## Replicate the analysis

```bash
$ make analyze
```

See the folder named `results` for the output:

- `plot.png` plots the simulation results
- `movers.csv` contains the conditional and hybrid estimates for the movers dataset
- `oa.csv` contains the conditional and hybrid estimates for the Opportunity Altas (OA) dataset

To replicate only the movers and OA analysis, use:

```bash
$ python src/analyze_data.py
```

To replicate only the simulation results analysis, use:

```bash
$ python src/analyze_simulations.py
```

## Replicate the simulations

You'll need a cluster environment with the `qsub` command to replicate the simulations.

### Setting up the cluster environment

This is a one-time setup.

1. Load your python version e.g., `module load python/python-3.8.5`. Change `cluster_setup.sh`, `simulate.sh`, and `cluster_teardown.sh` to load the correct python version.
2. In the root directory of the repo, create an environment with `python -m venv venv`
3. Activate the virtual environment `. venv/bin/activate`
4. Upgrade pip `python -m pip install -U pip`
5. Install the requirements `pip install -r requirements.txt`

### Set up

1. Login with `qlogin`
2. Run the cluster setup with `sh cluster_setup.sh`
3. Logout `logout`

### Run the simulations

4. Submit the simulations to the cluster with `qsub -t 1-$(wc -l < variables.txt) simulate.sh`

### Tear down

5. Login with `qlogin`
6. Tear down the cluster with `sh cluster_teardown.sh`
7. Logout with `logout`

The simulation results will be in `data/simulation_results.csv`.