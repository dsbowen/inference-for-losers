import importlib
import os

OUTPUT_DIR = "output"
RESULTS_DIR = "simulation_results"
SIMULATION_DIR = "src"
VARIABLES_IMPORT = "variables"
VARIABLES_TXT = "variables.txt"

if __name__ == "__main__":
    def get_line(args):
        try:
            return f"{' '.join([str(arg) for arg in args])}\n"
        except TypeError:
            # args is not iterable
            return f"{args}\n"

    # write variables to file
    variables = importlib.import_module(f".{VARIABLES_IMPORT}", package=SIMULATION_DIR)
    with open(VARIABLES_TXT, "w") as f:
        f.writelines([get_line(args) for args in variables.variables])

    # create directory for simulation results
    if not os.path.exists(RESULTS_DIR):
        os.mkdir(RESULTS_DIR)

    # create directory for cluster output
    if not os.path.exists(OUTPUT_DIR):
        os.mkdir(OUTPUT_DIR)
    