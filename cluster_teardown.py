import glob
import logging
import os
import shutil

import pandas as pd

from cluster_setup import OUTPUT_DIR, RESULTS_DIR, VARIABLES_TXT

DATA_DIR = "data"
FILENAME = "simulation_results.csv"

if __name__ == "__main__":
    # create a directory to store the results
    if not os.path.exists(DATA_DIR):
        os.mkdir(DATA_DIR)

    # concatenate and store the results
    dfs = []
    for path in glob.glob(os.path.join(RESULTS_DIR, "*.csv")):
        try:
            dfs.append(pd.read_csv(path))
        except:
            logging.warning(f"Error with file {path}")
    pd.concat(dfs).to_csv(os.path.join(DATA_DIR, FILENAME), index=False)

    # remove results files that are no longer needed]
    if os.path.exists(VARIABLES_TXT):
        os.remove(VARIABLES_TXT)
    if os.path.exists(RESULTS_DIR):
        shutil.rmtree(RESULTS_DIR)
    if os.path.exists(OUTPUT_DIR):
        shutil.rmtree(OUTPUT_DIR)
