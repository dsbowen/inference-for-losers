"""Plot simulation results.

Run with:

    .. code-block:: bash

        $ python src/analyze_simulations.py
"""
from __future__ import annotations

import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

DATA_FILE = os.path.join("data", "simulation_results.csv")
RESULTS_DIR = "results"


def clean(df: pd.DataFrame) -> pd.DataFrame:
    """Clean the dataset.

    Args:
        df (pd.DataFrame): Raw simulation results.

    Returns:
        pd.DataFrame: Cleaned simulation results.
    """
    df = df.rename(
        columns={"data_file": "Dataset", "rank": "Rank", "estimator": "Estimator"}
    )
    df.Dataset = df.Dataset.map({"oa": "OA", "movers": "Movers"})
    df.Estimator = df.Estimator.apply(lambda x: x.capitalize())
    max_rank = df.Rank.max()
    df["Rank"] = df.Rank.apply(lambda x: max_rank - x)

    df["Coverage"] = (df.ppf025 <= df.true_value) & (df.true_value <= df.ppf975)
    df["Probability bias"] = (df.params > df.true_value).astype(int) - 0.5
    df["Length CI"] = df.ppf975 - df.ppf025
    df["Error"] = df.params - df.true_value
    df["Absolute error"] = abs(df.params - df.true_value)

    return df


def analyze(df: pd.DataFrame) -> plt.figure.Figure:
    """Analyze and plot simulation results.

    Args:
        df (pd.DataFrame): Cleaned simulation results.

    Returns:
        plt.figure.Figure: Plot of results.
    """

    def add_row(
        row_number,
        y,
        share_y=False,
        include_projection=True,
        estimator=lambda x: x.mean(),
        y_label=None,
    ):
        kwargs = {
            "x": "Rank",
            "hue": "Estimator",
            "hue_order": ["Conventional", "Conditional", "Hybrid", "Projection"],
            "style": "Estimator",
            "style_order": ["Conventional", "Conditional", "Hybrid", "Projection"],
            "dashes": True,
            "ci": None,
            "legend": False,
            "estimator": estimator,
        }

        df = (
            movers_df
            if include_projection
            else movers_df[movers_df.Estimator != "Projection"]
        )
        movers_ax = sns.lineplot(data=df, y=y, ax=axes[row_number][0], **kwargs)

        if row_number == 0:
            kwargs["legend"] = True
        df = oa_df if include_projection else oa_df[oa_df.Estimator != "Projection"]
        oa_ax = sns.lineplot(data=df, y=y, ax=axes[row_number][1], **kwargs)
        oa_ax.set_ylabel(None)
        if row_number == 0:
            oa_ax.legend(bbox_to_anchor=(1, 1), loc="upper left")

        if y_label is not None:
            movers_ax.set_ylabel(y_label)

        if share_y:
            movers_ylim, oa_ylim = movers_ax.get_ylim(), oa_ax.get_ylim()
            ylim = min(movers_ylim[0], oa_ylim[0]), max(movers_ylim[1], oa_ylim[1])
            movers_ax.set_ylim(ylim)
            oa_ax.set_ylim(ylim)

        if row_number == 0:
            movers_ax.set_title("Dataset = Movers")
            oa_ax.set_title("Dataset = OA")

        if row_number < len(rows) - 1:
            movers_ax.set_xlabel(None)
            oa_ax.set_xlabel(None)

    # split into movers and OA (opportunity atlas)
    movers, oa = tuple(df.groupby("Dataset"))
    movers_df, oa_df = movers[1], oa[1]

    rows = [
        ("Coverage", {"share_y": True}),
        (
            "Length CI",
            {
                "estimator": lambda x: np.quantile(x, 0.5),
                "y_label": "Median length 95% CI",
            },
        ),
        (
            "Error",
            {
                "include_projection": False,
                "estimator": lambda x: np.quantile(x, 0.5),
                "y_label": "Median error",
            },
        ),
        (
            "Absolute error",
            {
                "include_projection": False,
                "estimator": lambda x: np.quantile(x, 0.5),
                "y_label": "Median abs error",
            },
        ),
    ]

    fig, axes = plt.subplots(len(rows), 2, figsize=(7, 10))
    fig.tight_layout()
    fig.legend(df.Estimator.unique())

    for i, (y, kwargs) in enumerate(rows):
        add_row(i, y, **kwargs)

    return fig


if __name__ == "__main__":
    sns.set()
    fig = analyze(clean(pd.read_csv(DATA_FILE)))
    if not os.path.exists(RESULTS_DIR):
        os.mkdir(RESULTS_DIR)
    fig.savefig(os.path.join(RESULTS_DIR, "plot.png"), bbox_inches="tight")
