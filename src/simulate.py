"""Run simulation.

Run with:

    .. code-block:: bash

        $ python src/simulate.py simulation_number estimator_name data_file_path

For example:

    .. code-block:: bash

        $ python src/simulate.py 0 conventional movers.csv
"""
from __future__ import annotations

import os
import sys

import numpy as np
import pandas as pd
from scipy.stats import multivariate_normal

from conditional_inference.bayes.classic import LinearClassicBayes
from conditional_inference.rqu import RQU

DATA_DIR = "data"
RESULTS_DIR = "simulation_results"
CONVENTIONAL = "conventional"
CONDITIONAL = "conditional"
HYBRID = "hybrid"
PROJECTION = "projection"


def run_simulation(estimator: str, df: pd.DataFrame) -> dict[str, np.ndarray]:
    """Run a simulation.

    Args:
        estimator (str): Name of the estimator to use.
        df (pd.DataFrame): Dataframe of conventional estimates

    Returns:
        dict[str, np.ndarray]: Mapping names of statistics to array of values.
    """
    true_mean, cov = df.values[:, 0], df.values[:, 1:]
    estimated_mean = multivariate_normal(true_mean, cov).rvs()
    argsort = estimated_mean.argsort()
    true_mean, estimated_mean, cov = (
        true_mean[argsort],
        estimated_mean[argsort],
        cov[argsort][:, argsort],
    )

    rqu = RQU(estimated_mean, cov)
    estimators = {
        CONVENTIONAL: lambda: LinearClassicBayes(
            estimated_mean, cov, prior_cov=np.inf
        ).fit(),
        CONDITIONAL: lambda: rqu.fit(),
        HYBRID: lambda: rqu.fit(beta=0.005),
        PROJECTION: lambda: rqu.fit(projection=True),
    }
    results = estimators[estimator]()
    conf_int = results.conf_int()
    results_df = pd.DataFrame(
        {
            "rank": np.arange(len(true_mean)),
            "true_value": true_mean,
            "params": results.params,
            "ppf025": conf_int[:, 0],
            "ppf975": conf_int[:, 1],
        }
    )
    results_df["estimator"] = estimator
    return results_df


if __name__ == "__main__":
    sim_no, estimator, data_file = sys.argv[1:]
    sim_no = int(sim_no)
    np.random.seed(sim_no)

    df = run_simulation(estimator, pd.read_csv(os.path.join(DATA_DIR, data_file)))
    df["sim_no"] = sim_no
    df["data_file"] = data_file[: -len(".csv")]
    if not os.path.exists(RESULTS_DIR):
        os.mkdir(RESULTS_DIR)
    filename = os.path.join(
        RESULTS_DIR, f"sim_no={sim_no}-estimator={estimator}-data_file={data_file}.csv"
    )
    df.to_csv(filename, index=False)
