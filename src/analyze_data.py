"""Analyze data for empirical applications.

Run with:

    .. code-block:: bash

        $ python src/analyze_data.py
"""
from __future__ import annotations

import os
from io import StringIO

import numpy as np
import pandas as pd
from conditional_inference.rqu import RQU

DATA_PATH = "data"
DATA_FILENAMES = ("movers.csv", "oa.csv")
RESULTS_DIR = "results"


def analyze(filename: str, use_hybrid: bool) -> pd.DataFrame:
    """Analyze a data file.

    Args:
        filename (str): Name of the data file.
        use_hybrid (bool): Indicates to use the hybrid estimator (as opposed to the
            conditional estimator).

    Returns:
        pd.DataFrame: Results.
    """
    results = RQU.from_csv(filename).fit(cols="sorted", beta=0.005 if use_hybrid else 0)
    table = results.summary()
    df = pd.read_csv(StringIO(table.as_csv()), skiprows=1).dropna()
    df = df.rename(columns={df.columns[0]: "CZ"})
    argsort = (-results.model.mean).argsort()
    df["Naive Estimate"] = results.model.mean[argsort]
    df["SE"] = np.sqrt(results.model.cov.diagonal())[argsort]
    df["Rank"] = df.index + 1
    return df.drop(columns="pvalue")


if __name__ == "__main__":
    if not os.path.exists(RESULTS_DIR):
        os.mkdir(RESULTS_DIR)

    merge_on = ["Rank", "CZ", "Naive Estimate", "SE"]
    for filename in DATA_FILENAMES:
        file_path = os.path.join(DATA_PATH, filename)
        df = pd.merge(
            analyze(file_path, False),
            analyze(file_path, True),
            on=merge_on,
            suffixes=("_conditional", "_hybrid"),
        )
        df = df[merge_on + [col for col in df.columns if col not in merge_on]]
        df.to_csv(os.path.join(RESULTS_DIR, filename), index=False)
        df.to_latex(
            os.path.join(RESULTS_DIR, f"{filename[:-len('.csv')]}.txt"), index=False
        )
